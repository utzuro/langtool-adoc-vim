let s:scriptpath = expand('<sfile>:p:h') . '/../langtool-vim.py '

function! LanguageToolFunc()
    setlocal errorformat=LT\ '%f':%l:%v:\ %m
    let &l:makeprg = s:scriptpath . expand('%')
    make
endfunction

command LanguageTool :call LanguageToolFunc()
